import logo from './logo.svg';
import './App.css';
import { realm } from './config';
import { Switch } from 'react-router';
import AppIndex from './apps';
import { ReactKeycloakProvider } from '@react-keycloak/web';
import keycloak, { keycloakConfig } from './apps/module/keycloak.config';
import React from 'react';
import { GlobalProvider } from './apps/context';


function App() {
  return (
    <GlobalProvider>
      <AppIndex />
    </GlobalProvider>
  );
}

export default App;
