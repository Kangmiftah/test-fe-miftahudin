import React, { useState } from 'react';

export default ({url, param = {}, options = {}}) => {
    const [response, setResponse] = React.useState(null);
    const [error, setError] = React.useState(null);
    const [refresh, setRefresh] = useState(true)
    const headers = { 
        "X-Api-Key" : "6a0230af7ed84bd897a31f6cbfeb5782"
    }
    React.useEffect(() => {
        if (options.headers === undefined){
            options = { ...options, headers }
        }else{
            options = { ...options, headers : { ...options.headers, ...headers } }
        }

        if (param != null) {
            var keys = Object.keys(param)
            keys.forEach((val, key) => {
                if (key == 0)
                    url = url + "?"
                url = url + (val + "=" + param[val])
                if (key != (keys.length - 1))
                    url = url + "&"
            })
        }
      const fetchData = async () => {
        try {
          const res = await fetch(url, options);
          const json = await res.json();
          setResponse(json);
        } catch (error) {
          setError(error);
        }
      };
      refresh && fetchData();
      setRefresh(false)
      console.log(refresh)
    }, [refresh]);
  
    const refreshFetch = ()=>setRefresh(true)
    return { response, error, refreshFetch };
  };