import React from 'react';
import { UseGlobalState } from '../../context';

const SearchBar = () => {
    const { globalState, actions } = UseGlobalState();

    const { filter = {} } = globalState
    return ( 
        <>
            <div className="card">
                <div className="card-body">
                    <div className="form-group">
                        <label>
                            Search
                        </label>
                        <div className="input-group">
                            <input className="form-control" 
                                value={filter.q} onChange={(e)=>{
                                   actions.setState({filter: { ...filter, q:e.target.value}}) 
                                }}
                                placeholder="search" 
                            />
                            <div className="input-group-append">
                               <select value={filter.category} onChange={(e)=>{
                                   actions.setState({ filter: {...filter, category:e.target.value}})
                               }}  className="form-control">
                                   <option value="">All Categories</option>
                                   <option value="business">Business</option>
                                   <option value="science">Science</option>
                                   <option value="technology">Technology</option>
                               </select>
                            </div>
                            <div class="input-group-append">
                                <button className="btn btn-secondary">
                                    <i className="fas fa-fw fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
     );
}
 
export default SearchBar;