import React from "react";
import { Link } from "react-router-dom";
import configLayout from "../../../layout.config";

const Navbar = () => {
    return (
        <>
            {/* <!-- Navbar --> */}
            <nav className={`main-header navbar ${configLayout.navbarClass}`}>
                <div className="container">
                    <div className="collapse navbar-collapse order-3" id="navbarCollapse">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link to="/" href="index3.html" className="nav-link">
                                    Home
                                </Link>
                            </li>

                            {/* <li className="nav-item dropdown">
                                <a 
                                id="dropdownSubMenu1" 
                                href="#" 
                                data-toggle="dropdown" 
                                aria-haspopup="true" 
                                aria-expanded="false" 
                                className="nav-link dropdown-toggle"
                                >Category</a>
                                 <ul aria-labelledby="dropdownSubMenu1" className="dropdown-menu border-0 shadow">
                                    <li><a href="#" className="dropdown-item">Some action </a></li>
                                    <li><a href="#" className="dropdown-item">Some other action</a></li>
                                </ul>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </nav>
            {/* <!-- /.navbar --> */}
        </>
    );
};

export default Navbar;
