import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { LoadingIcon } from '../components';
import { Dashboard, Test, ReadNews } from '../pages';


function Fallback(){
    return(
        <div style={{
            margin:"auto",
            width: "fit-content",
            paddingTop: "20%",
            textAlign:"center"
        }}>
            <LoadingIcon width="100px" />
            <h5 className="text-base">Loading</h5>
        </div>
    )
}

export default function (){

    return(
        <>
            <Switch>
                <Suspense fallback={<Fallback />}>
                    <Route
                        key="/"
                        path="/"
                        exact={true}
                        render={(props)=><Dashboard { ...props} />}
                    />
                    <Route
                        key="/read-news"
                        path="/read-news"
                        exact={true}
                        render={(props)=> <ReadNews { ...props} />}
                    />
                </Suspense>
            </Switch>
        </>
    )
}