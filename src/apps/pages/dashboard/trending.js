import React, { useEffect } from "react";
import { UseGlobalState } from "../../context";
import moment from "moment";
import UseDashboard from "./useDashboard";
import { SkeletonCustom } from "../../components";
import { Link } from "react-router-dom";

const Trending = () => {
    const { globalState } = UseGlobalState();
    const { trendingsData = [], filter, trendingLoading=true } = globalState;
    const { dashboardState, dashboardActions} = UseDashboard();
    useEffect(()=>{
        dashboardActions.getTrending()
    }, [filter.category])
    return (
        <>
            <div className="card card-primary card-outline">
                <div className="card-body">
                    <h5>Top Headlines </h5>
                    <div className="row">
                        {
                            trendingLoading ? [1,2,3].map((v, k)=> (
                                <div key={k} className="col-md-4">
                                    <SkeletonCustom typ="card" width="100%" />
                                </div>
                            )) : trendingsData.map(
                                (
                                    article,
                                    key
                                ) => {
                                    const {
                                        author,
                                        content,
                                        description,
                                        publishedAt,
                                        title,
                                        url,
                                        urlToImage,
                                    } = article;
                                    return (
                                        <div key={key} className="col-md-4">
                                            <div className="card mb-2">
                                                <img
                                                    className="card-img-top"
                                                    style={{minHeight: "150px", maxHeight:"180px"}}
                                                    src={urlToImage}
                                                />
                                                <div className="card-img-overlay d-flex flex-column justify-content-end">
                                                    <h5 className="card-title text-white" style={{padding:10, background:"rgba(0,0,0,0.5)", borderRadius:5}}>
                                                        {title}
                                                        
                                                    </h5>
                                                    <Link to={{
                                                        pathname:"/read-news",
                                                        state:{ news: article }    
                                                    }}>Read More</Link>
                                                    {/* <p className="card-text text-white pb-2 pt-1">
                                                        {description}
                                                    </p> */}
                                                    <a href="#">
                                                        {moment(publishedAt).format("DD/MM/YYYY HH:ss")}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Trending;
