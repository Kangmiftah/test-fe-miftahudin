import React, { useCallback, useState } from 'react';
import { UseGlobalState } from '../../context';

const UseDashboard = () => {

    const {globalState, actions } = UseGlobalState();
    const [ category, setCategory ] = useState(""); 
    const [ page, setPage] = useState(1);
    const [ maxPage, setMaxPage] = useState();

    const getTrending = useCallback((
        _var= "trendingsData", 
        _loadingVar ="trendingLoading", 
        filter={
            q:""
        },
        pageSize= 3, 
        setupMaxpage=false
    ) => {
        actions.getData({
            url: "https://newsapi.org/v2/top-headlines",
            param: { ...globalState.filter, pageSize, ...filter, page },
            _var,
            _loadingVar,
            _varResp: "articles",
            sortBy: 'publishedAt',
        }).then(function(resp){
            if(setMaxPage){ 
                const totalMax = (resp.totalResults/pageSize);
                setMaxPage(totalMax);
            }
        });
    }, [globalState.trendingsData])

    const nextPage = useCallback(()=>{
        setPage(prevPage => prevPage+1); 
    }, [page])

    const prevPage = useCallback(()=>{
        page > 1 && setPage(prevPage => prevPage-1); 
    }, [page])

    const getLatest = useCallback(() => {
        actions.getData({
            url: "https://newsapi.org/v2/top-headlines",
            param: { ...globalState.filter, pageSize: 3, q:"", sortBy: 'publishedAt', category,  country:"id" },
            _var : "latestData",
            _loadingVar : "latestLoading",
            _varResp: "articles",
        })    
    }, [globalState.latestData])

    const search = useCallback(function(){
        if(page == 1)
        {
            setPage(0);
            setTimeout(function(){
                setPage(1);
            },100)
        }else{
            setPage(1);
        }
    }, [page])

    return {
        dashboardState:{
            category,
            page,
            maxPage
        },
        dashboardActions:{
            getTrending,
            getLatest,
            setCategory,
            nextPage,
            prevPage,
            search
        }
    }
}
 
export default UseDashboard;