import React, { useEffect, useState } from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { baseUrl } from '../../../config';
import { Button } from 'react-bootstrap';
import Layouts from "../../layouts";
import { LoadingIcon, SkeletonCustom } from '../../components';
import { UseGlobalState } from '../../context';
import Trending from './trending';
import Updated from './udpated';
import UseDashboard from './useDashboard';
import './style.css';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default function () {
    const { globalState, actions } = UseGlobalState();
    const { dashboardState, dashboardActions } = UseDashboard();
    const { dashboardData = [], loadingDashboard = true } = globalState;

    useEffect(() => {
        dashboardActions.getTrending(
            "dashboardData",
            "loadingDashboard",
            globalState.filter,
            10,
            true
        )
    }, [ dashboardState.page ])
    return (
        <>
             <div className="card">
                <div className="card-body">
                    <div className="form-group">
                        <label>
                            Search
                        </label>
                        <div className="input-group">
                            <input className="form-control" 
                                value={ globalState.filter.q} onChange={(e)=>{
                                   actions.setState({filter: { ...globalState.filter, q:e.target.value}}) 
                                }}
                                placeholder="search" 
                            />
                            <div className="input-group-append">
                               <select value={globalState.filter.category} onChange={(e)=>{
                                   actions.setState({ filter: {...globalState.filter, category:e.target.value}})
                               }}  className="form-control">
                                   <option value="">All Categories</option>
                                   <option value="business">Business</option>
                                   <option value="science">Science</option>
                                   <option value="technology">Technology</option>
                               </select>
                            </div>
                            <div class="input-group-append">
                                <button onClick={dashboardActions.search} className="btn btn-secondary">
                                    <i className="fas fa-fw fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Trending />
            <div className="row">
                <div className="col-md-9">
                    <div className="card card-primary  card-outline">
                        <div className="card-header">
                            News
                        </div>
                        <div className="card-body" style={{ minHeight: 300 }}>
                            {
                                loadingDashboard ? [1,2,3,4,5].map((v, k)=> (
                                    <div key={k} className="col-md-12">
                                        <SkeletonCustom typ="card" width="100%" />
                                    </div>
                                )) : dashboardData.map((article, i) => {
                                    const {
                                        author,
                                        content,
                                        description,
                                        publishedAt,
                                        title,
                                        url,
                                        urlToImage,
                                    } = article;
                                    return (
                                        <div className="row mb-3">
                                            <div className="col-md-12">
                                                <div className="card" style={{ 
                                                    boxShadow: "none", 
                                                    borderBottom: "1px solid rgba(0,0,0,0.1)",

                                                }}>
                                                    <div className="card-body" style={{ padding: 0, paddingBottom:10 }}>
                                                        <div className="row">
                                                            <div className="col-md-3">
                                                                <img src={urlToImage} style={{width:"100%"}} />
                                                            </div>
                                                            <div className="col-md-9">
                                                                <h6>
                                                                    {title}
                                                                </h6>
                                                                <p>
                                                                    {description} <Link to={{
                                                                        pathname:"/read-news",
                                                                        state:{ news: article }    
                                                                    }}>Read More</Link>
                                                                </p>

                                                                <span>
                                                                  <i className="fa fa-clock"></i>  Updated : {moment(publishedAt).format("DD/MM/YYYY HH:ss")}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                            <div className="row">
                                <div className="col-md-12">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination" style={{
                                            margin: "auto",
                                            width: "fit-content"
                                        }}>
                                            <li class="page-item"><a class="page-link" onClick={dashboardActions.prevPage} href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">{dashboardState.page}</a></li>
                                            <li class="page-item"><a class="page-link" onClick={dashboardActions.nextPage} href="#">Next</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="card  card-outline card-primary">
                        <div className="card-header text-center">
                            Latest News
                        </div>
                        <div className="card-body" style={{ minHeight: 300 }}>
                            <Updated />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}