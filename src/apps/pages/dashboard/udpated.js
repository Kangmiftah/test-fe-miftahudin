import moment from 'moment';
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { SkeletonCustom } from '../../components';
import { UseGlobalState } from '../../context';
import UseDashboard from './useDashboard';

const Updated = () => {

    const { dashboardState, dashboardActions } = UseDashboard();
    const { globalState } = UseGlobalState();
    const { latestData = [],  latestLoading} = globalState;

    useEffect(()=>{
        dashboardActions.getLatest();
    },[dashboardState.category])

    return ( 
        <>
            <div className="form-group">
                <select value={dashboardState.category} onChange={(e)=> dashboardActions.setCategory(e.target.value)} className="form-control">
                    <option value="">All Categories</option>
                    <option value="business">Business</option>
                    <option value="science">Science</option>
                    <option value="technology">Technology</option>
                </select>
            </div>

            {
                latestLoading ? [1,2,3].map((v, k)=> (
                    <div key={k} className="col-md-12">
                        <SkeletonCustom typ="card" width="100%" />
                    </div>
                )) : latestData.map( (
                    article,
                    key
                ) => {
                    const {
                        author,
                        content,
                        description,
                        publishedAt,
                        title,
                        url,
                        urlToImage,
                    } = article; 
                    return (
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card" style={{boxShadow:"none", borderBottom:"1px solid rgba(0,0,0,0.1)"}}>
                                    <div className="card-body" style={{padding:0}}>
                                        <img src={urlToImage} style={{width:"100%"}} />
                                        <div style={{marginBottom: "17px",fontSize: "xx-small"}}>
                                            {moment(publishedAt).format("DD/MM/YYYY HH:ss")}
                                        </div>
                                        <div className="elipsis">
                                            <h6>{title}</h6>
                                        </div>
                                        <Link to={{
                                                                        pathname:"/read-news",
                                                                        state:{ news: article }    
                                                                    }}>Read More</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </>
    );
}
 
export default Updated;