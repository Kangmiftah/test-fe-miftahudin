import moment from 'moment';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';

export default function ({location}) {

    const history = useHistory();

    useEffect(()=>{
        console.log(location.state)
        if(location.state === null || location.state === undefined)
        {
            history.goBack();
        }
    },[])
    const { news } = location.state
    const {
        author,
        content,
        description,
        publishedAt,
        title,
        url,
        urlToImage,
    } = news;
    return (
        <>

            <div className="row mb-3">
                <div className="col-md-12">
                <div className="card card-primary  card-outline">
                    
                        <div className="card-header">
                            <button className="btn btn-primary" onClick={()=>history.goBack()}>
                                <i className="fa fa-arrow-left"></i> Back
                            </button>
                        </div>

                        <div className="card-body" style={{ paddingBottom: 10 }}>
                            <span>
                                <i className="fa fa-clock"></i>  Updated At {moment(publishedAt).format("DD/MM/YYYY HH:ss")}
                            </span>

                            
                            <span className="ml-3">
                                <i className="fa fa-edit"></i>  Updated By {author}
                            </span>
                            <h2 style={{marginTop:5}}>{title}</h2>

                            <img src={urlToImage} style={{ width:"100%", marginTop:20, marginBottom:20 }}/>
                            <p>
                                {description}
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
}