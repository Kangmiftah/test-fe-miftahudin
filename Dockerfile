FROM node:alpine as builder

WORKDIR /app

COPY package.json /app

RUN npm install

CMD ["npm", "start"]